const  map = [  
    "  WWWWW ",  
    "WWW   W ",  
    "WOSB  W ",  
    "WWW BOW ",  
    "WOWWB W ",  
    "W W O WW",  
    "WB XBBOW",  
    "W   O  W",  
    "WWWWWWWW"  
  ]; 

let currentColumn = 0;
let currentRow = 9;
let playerPosition = {row:2, column:2}
const maze = document.getElementById("container")

for (let rowIndex = 0; rowIndex < map.length; rowIndex += 1) {
    let mapRow = map[rowIndex]
    var row = document.createElement("div");
    row.dataset.rowIndex = rowIndex;
    row.className = "row";
    for (let columnIndex = 0; columnIndex < mapRow.length; columnIndex += 1){
        var column = document.createElement("div");
        column.dataset.columnIndex = columnIndex;
        column.dataset.rowIndex = rowIndex;
        column.className = "column";
        switch (mapRow[columnIndex]) {
            case "S":
            column.id = "start"
            playerPosition.row = rowIndex
            playerPosition.column = columnIndex
            break

            case "W":
            column.id = "wall"
            break

            case "F":
            column.id= "finish"
            break

            case "O":
            column.id = "empty_storage"
            break

            case "B":
            column.id = "box_start"
            break

            case "X":
            column.id = "box_stored"
            
            break 

            case " ":
            column.id= "space"
            break
        
        }

        row.appendChild(column);
    }
    maze.appendChild(row);
}

document.addEventListener('keydown', movePlayer);

let player = document.createElement("div")
player.classList.add("player")
document.getElementById("start").appendChild(player)

function movePlayer(e) {
    e = event.keyCode

    const rightCell = document.querySelector("[data-row-index ='" +playerPosition.row+"'][data-column-index = '"+(playerPosition.column+1)+"']")
    const nextRightCell = document.querySelector("[data-row-index ='" +playerPosition.row+"'][data-column-index = '"+(playerPosition.column+2)+"']")
    const leftCell = document.querySelector("[data-row-index ='" +playerPosition.row+"'][data-column-index = '"+(playerPosition.column-1)+"']")
    const nextLeftCell = document.querySelector("[data-row-index ='" +playerPosition.row+"'][data-column-index = '"+(playerPosition.column-2)+"']")
    const topCell = document.querySelector("[data-row-index ='" +(playerPosition.row-1)+"'][data-column-index = '"+(playerPosition.column)+"']")
    const nextTopCell = document.querySelector("[data-row-index ='" +(playerPosition.row-2)+"'][data-column-index = '"+(playerPosition.column)+"']")
    const bottomCell = document.querySelector("[data-row-index ='"+(playerPosition.row+1)+"'][data-column-index = '"+(playerPosition.column)+"']")
    const nextBottomCell = document.querySelector("[data-row-index ='" +(playerPosition.row+2)+"'][data-column-index = '"+(playerPosition.column)+"']")
    
    if (e == 39) {
        if (rightCell.id === "wall") {
            return;
        }

        else if (rightCell.id === "box_start") {

            if (nextRightCell.id === "wall") {
                return;
            }

            else if (nextRightCell.id === "box_start") {
                return;
            }

            else if (nextRightCell.id === "box_stored") {
                return;
            }

            else if (nextRightCell.id === "empty_storage") {
                nextRightCell.id = "box_stored"
                rightCell.id = "space"
            }

            

            else if (nextRightCell.id === "space") {
                nextRightCell.id = "box_start"
                rightCell.id = "space"
            }

        }

        else if (rightCell.id === "box_stored") {

            if (nextRightCell.id === "wall") {
                return;
            }

            if (nextRightCell.id === "box_start") {
                return;
            }

            nextRightCell.id = "box_start"
            rightCell.id = "empty_storage"
        }

        rightCell.appendChild(player);
        playerPosition.column += 1;  
    }

    if (e == 38) {
        if (topCell.id === "wall") {
            return;
        }

        else if (topCell.id === "box_start") {

            if (nextTopCell.id === "wall") {
                return;
            }

            else if (nextTopCell.id === "box_start") {
                return;
            }

            else if (nextTopCell.id === "empty_storage") {
                nextTopCell.id = "box_stored"
                topCell.id = "space"
            }

            else if (nextTopCell.id === "space") {
                nextTopCell.id = "box_start"
                topCell.id = "space" 
            }
        }

        else if (topCell.id === "box_stored") {

            if (nextTopCell.id === "wall") {
                return;
            }

            if (nextTopCell.id === "box_start") {
                return;
            }

            nextTopCell.id = "box_start"
            topCell.id = "empty_storage"
        }

        topCell.appendChild(player)
        playerPosition.row += -1;

    }

    if (e == 40) {
        if (bottomCell.id === "wall") {
            return;
        }

        else if (bottomCell.id === "box_start") {

            if (nextBottomCell.id === "wall") {
                return;
            }

            else if (nextBottomCell.id === "box_start") {
                return;
            }

            else if(nextBottomCell.id === "empty_storage") {
                nextBottomCell.id = "box_stored"
                bottomCell.id = "space"
            }

            else if (nextBottomCell.id === "space" ) {
                nextBottomCell.id = "box_start"
                bottomCell.id = "space"
            }
            
        }

        else if (bottomCell.id === "box_stored") {
            if (nextBottomCell.id === "wall") {
                return;
            }

            if (nextBottomCell.id === "box_start") {
                return;
            }
            nextBottomCell.id = "box_start"
            bottomCell.id = "empty_storage"
        }

        bottomCell.appendChild(player)
        playerPosition.row += 1;
        
    }

    if (e == 37) {
        if (leftCell.id === "wall") {
            return;
        }

        else if (leftCell.id === "box_start") {

            if (nextLeftCell.id === "wall") {
                return;
            }

            else if (nextLeftCell.id === "box_start") {
                return;
            }

            else if (nextLeftCell.id === "box_stored") {
                return;
            }
            
            else if (nextLeftCell.id === "empty_storage") {
                nextLeftCell.id = "box_stored"
                leftCell.id = "space"
            }

            else if (nextLeftCell.id === "space" || "start") {
                nextLeftCell.id = "box_start"
                leftCell.id = "space"
            }

            
        }

        else if (leftCell.id === "box_stored") {
            if (nextLeftCell.id === "wall") {
                return;
            }

            if (nextLeftCell.id === "box_start") {
                return;
            }
            nextLeftCell.id = "box_start"
            leftCell.id = "empty_storage"
        }

        leftCell.appendChild(player)
        playerPosition.column += -1;
        
    }

   checkWin()
}

function checkWin() {
    let boxCount = document.querySelectorAll("#box_stored").length;
    console.log(boxCount)

    if (boxCount === 7) {
        let winDiv = document.getElementById("winText");
        let winText = document.createTextNode("You Did It!");
        winDiv.appendChild(winText)
        console.log("You Win!")
        document.removeEventListener('keydown', movePlayer);
    }

}